;(function ( $ ){
  $( document ).ready( function (){
		var show = true,
						durationShow = 100,
						durationHide = 100;	
			
			 /* Событие: показать меню */
			 $( '.show-hide-menu' ).click( function(){
					 /* Показвать */
					 if ( show ) {
        $('.menu-navigation').find('li').each(function(i, e) {
								  $(this).css({ 'transition' : 'all .5s' });
									 $(this).stop(true, true).animate({
										  'height' : '57px'
										}, durationShow * i, function() {
										  $(this).css({
												  'transform' : 'rotateY(0deg)'
												});
										});
								});
						  show = false;
						} else {
							 /* Спрятать */
						  $('.menu-navigation').find('li').each(function(i, e) {
									 var $this = $(this);
								  $(this).stop(true, true).animate({
										  'height' : '0px',
										}, durationHide * i, function () {
										  $(this).css({
										    'transform' : 'rotateY(90deg)'
										  });
										});
								});
							 show = true;
						}
				});
		
		/* На ресайз очищать стили */
  $(window).resize(function () {
		  if ( $(this).width() >= 1000 ) {
				  $('.menu-navigation').find('li').removeAttr('style');
					 show = true;
				} 
		});
			
		});
})( jQuery );